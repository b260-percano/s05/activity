<?php

$tasks = ['Test0', 'Test1', 'Test2', 'Test3'];

if(isset($_GET['index'])){
  $indexGet = $_GET['index'];
  echo "Task from GET is $tasks[$indexGet]";
}

if(isset($_POST['index'])){
  $indexPost = $_POST['index'];
  echo "Task from POST is $tasks[$indexPost]";
}


?>


<!DOCTYPE html>

<html>
<head>

  <title>S05: Client-Server Communication (GET and POST)</title>

</head>

<body>
  <h1>Task index from GET</h1>

  <form method="GET">
    <select name="index" required>
      <option value="0">0</option>
      <option value="1">1</option>
      <option value="2">2</option>
      <option value="3">3</option>
    </select>
    <button type="submit">GET</button>
  </form>

  <h1>Task index from POST</h1>

  <form method="POST">
    <select name="index" required>
      <option value="0">0</option>
      <option value="1">1</option>
      <option value="2">2</option>
      <option value="3">3</option>
    </select>
    <button type="submit">POST</button>
  </form>

</body>
</html>