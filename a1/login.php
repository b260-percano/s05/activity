<?php
session_start();
require 'code.php';


if (isset($_GET["submit"])) {

  if ($email = 'johnsmith@gmail.com' && $password = '1234') {
    header("Location: index.php");
  } else if ($email !== 'johnsmith@gmail.com' && $password !== '1234') {
    echo
    "<script> alert('Password Incorrect'); </script>";
  }
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <link rel="stylesheet" href="style.css">
  <link rel="icon" href="./img/oMEN ICON.jpg" type="image/x-icon">
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Login</title>
</head>

<body>
  <div>
    <h2>LOGIN</h2>
  </div>
  <div>
    <form method="GET" autocomplete="off">
      <div class="card-body">
        <label for="email">Email:</label>
        <input required type="email" name="email" class="form-control mb-3" placeholder="Enter Email">
        <div class="form-group">
          <label for="password">Password:</label>
          <input required name="password" type="password" placeholder="Enter Password">
        </div>
        <button type="submit" name="submit">LOGIN</button>
      </div>
    </form>
  </div>

</body>

</html>