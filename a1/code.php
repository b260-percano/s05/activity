<?php 
class Login {

  public $email;
  public $password;

  public function __construct($email,$password){
      $this->email = $email;
      $this->password = $password;

  }

  public function getEmail(){
    return $this->email;
  }

  public function getPassword(){
    return $this->password;
  }

}

$login = new Login('johnsmith@gmail.com', '1234');

?>